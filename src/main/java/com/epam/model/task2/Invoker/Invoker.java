package com.epam.model.task2.Invoker;

import com.epam.model.task2.Command;

public class Invoker {
    private Command openText;
    private Command editText;
    private Command saveText;
    private Command closeText;

    public Invoker(Command open, Command edit, Command save, Command close )
    {
        this.openText = open;
        this.editText = edit;
        this.saveText = save;
        this.closeText = close;
    }

    public void pressOpen()
    {
        openText.execute();
    }

    public void pressEdit()
    {
        editText.execute();
    }

    public void pressSave()
    {
        saveText.execute();
    }

    public void pressClose()
    {
        closeText.execute();
    }
}
