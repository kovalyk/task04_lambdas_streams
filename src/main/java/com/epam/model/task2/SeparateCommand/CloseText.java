package com.epam.model.task2.SeparateCommand;

import com.epam.model.task2.Command;
import com.epam.model.task2.Receiver.Text;

public class CloseText implements Command {
    private Text text;

    public CloseText( Text text )
    {
        this.text = text;
    }

    @Override
    public void execute()
    {
        text.close();
    }
}
