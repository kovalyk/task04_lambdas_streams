package com.epam.model.task2.SeparateCommand;

import com.epam.model.task2.Command;
import com.epam.model.task2.Receiver.Text;

public class OpenText implements Command {

    private Text text;

    public OpenText(Text text) {
        this.text = text;
    }

    @Override
    public void execute() {
        text.open();
    }
}

