package com.epam.model.task2.SeparateCommand;

import com.epam.model.task2.Command;
import com.epam.model.task2.Receiver.Text;

public class SaveText implements Command {
    private Text text;

    public SaveText( Text text )
    {
        this.text = text;
    }

    @Override
    public void execute()
    {
        text.save();
    }
}
