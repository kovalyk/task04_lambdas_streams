package com.epam.model.task2.Receiver;

public class Text {
    public void open()
    {
        System.out.println("Text is Opened");
    }

    public void edit()
    {
        System.out.println("Text is Edited");
    }

    public void save()
    {
        System.out.println("Text is Saved");
    }

    public void close()
    {
        System.out.println("Text is Closed");
    }

}
