package com.epam.model.task2;

@FunctionalInterface
public interface Command {
    void execute();
}
