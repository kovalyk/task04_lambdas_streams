package com.epam.model.task2;

import com.epam.model.task2.Invoker.Invoker;
import com.epam.model.task2.Receiver.Text;
import com.epam.model.task2.SeparateCommand.CloseText;
import com.epam.model.task2.SeparateCommand.EditText;
import com.epam.model.task2.SeparateCommand.OpenText;
import com.epam.model.task2.SeparateCommand.SaveText;

public class Application {
    public static void main( String[] args )
    {
        Text text = new Text();
        Command openText = new OpenText(text);
        Command editText = new EditText(text);
        Command saveText = new SaveText(text);
        Command closeText = new CloseText(text);
        Invoker option = new Invoker(openText,editText, saveText,closeText);
        option.pressOpen();
        option.pressEdit();
        option.pressSave();
        option.pressClose();
    }
}
