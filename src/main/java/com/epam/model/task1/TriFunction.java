package com.epam.model.task1;

@FunctionalInterface
public interface TriFunction {
    int calculate(int num1, int num2, int num3);
}
