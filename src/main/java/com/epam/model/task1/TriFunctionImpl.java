package com.epam.model.task1;

public class TriFunctionImpl {
   public TriFunction getMax() {
        return (a, b, c) -> Math.max(a, Math.max(b, c));
    }

   public TriFunction getAverage() {
        return (a, b, c) -> ((a + b + c) / 3);
    }
}
