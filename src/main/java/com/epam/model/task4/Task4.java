package com.epam.model.task4;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Task4 {
    private static List<String> stringList = new ArrayList<>();

    public void getResult() {
        System.out.println("Enter some number of text lines:");
        Scanner scanner = new Scanner(System.in);
        String s;
        while (true) {
            s = scanner.nextLine();
            if (s.isEmpty()) {
                break;
            }
            stringList.add(s);
        }
        countUniqueWords();
        countWordOccurrence();
    }

    private void countUniqueWords() {
        System.out.println("Number of unique words: "
                + stringList.stream().distinct().count());
        System.out.println("Sorted list of all unique words: ");
        stringList.stream().distinct()
                .sorted().forEach(x -> System.out.println(x + " "));
    }

    private void countWordOccurrence() {
        System.out.println("\nOccurrence of each word in the text: "
                + stringList.stream().collect(Collectors
                .groupingBy(x -> x, Collectors.counting())));

        System.out.println("Occurrence number of each symbol except upper case characters: "
                + stringList.stream().flatMap(x -> x.chars().boxed())
                .map(x -> (char) x.intValue()).collect(Collectors
                        .groupingBy(x -> x, Collectors.counting())));
    }
}
