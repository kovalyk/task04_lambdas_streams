package com.epam.controller;

import com.epam.model.task1.TriFunctionImpl;
import com.epam.model.task2.Command;
import com.epam.model.task2.Invoker.Invoker;
import com.epam.model.task2.Receiver.Text;
import com.epam.model.task2.SeparateCommand.CloseText;
import com.epam.model.task2.SeparateCommand.EditText;
import com.epam.model.task2.SeparateCommand.OpenText;
import com.epam.model.task2.SeparateCommand.SaveText;
import com.epam.model.task3.RandomInteger;
import com.epam.model.task4.Task4;
import com.epam.view.View;

public class Controller {

    View view = new View();
    private String result;

    public final void getMax() {
        TriFunctionImpl triFunction = new TriFunctionImpl();
        result = "Maximum value of numbers (1, 5, 10): ";
        updateView(triFunction.getMax().calculate(1, 5, 10));
    }

    public final void getAverage() {
        TriFunctionImpl triFunction = new TriFunctionImpl();
        result = "Average result of numbers (2, 15, 35): ";
        updateView(triFunction.getAverage().calculate(2, 15, 35));
    }

         public final void getTask2() {
             Text text = new Text();
             Command openText = new OpenText(text);
             Command editText = new EditText(text);
             Command saveText = new SaveText(text);
             Command closeText = new CloseText(text);
             Invoker option = new Invoker(openText,editText, saveText,closeText);
             option.pressOpen();
             option.pressEdit();
             option.pressSave();
             option.pressClose();
     }

    public final void getTask3() {
        RandomInteger randomInteger = new RandomInteger();
        randomInteger.implement();
    }
    public final void getResult() {
        Task4 taskD = new Task4();
        taskD.getResult();
    }

    private void updateView(int lambdaResult) {
        view.printResult(lambdaResult, result);
    }

}
