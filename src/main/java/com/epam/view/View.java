package com.epam.view;

import com.epam.controller.Controller;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {

    private static Scanner input = new Scanner(System.in);
    private Controller controller = new Controller();
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public View() {
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - print Task1");
        menu.put("2", "  2 - print Task2");
        menu.put("3", "  3 - print Task3");
        menu.put("4", "  4 - print Task4");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
    }

    private void pressButton1() {
        controller.getMax();
        controller.getAverage();
    }

    private void pressButton2() {
        controller.getTask2();
    }

    private void pressButton3() {
        controller.getTask3();
    }

    private void pressButton4() {
        controller.getResult();
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public final void printResult(int numResult, String result) {
        System.out.println(result);
        System.out.println(numResult);
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}


